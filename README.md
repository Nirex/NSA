NSA
===

NULL STORAGE API

Copyright (c) 2017 Nirex

WELCOME
=======

Welcome to NSA

Please don't forget to read the file LICENSE

INTRODUCTION
============

NSA is a C# library. NSA will allow you to save/load your custom data on demand.

Please report all problems or suggestions to the author. Thanks.

COPYRIGHT
=========

Copyright (C) 2017 Nirex

Please read the LICENSE file for License details and terms of use.

USED TERMS
==========

[ The term NSA is a shorthand for the Null Storage API and holds no connection with potential owners of registered trademarks or other rights. ]

CONTACT
=======

Nirex.0@gmail.com
