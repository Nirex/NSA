﻿/**************************************************************************\
Copyright (c) 2017 Nirex.0@Gmail.Com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
\**************************************************************************/

using System;
using System.IO;
using System.Text;

/// <summary>
/// Core NSA Namespace.
/// </summary>
namespace NSA.Storage
{
    /// <summary>
    /// Loader Class.
    /// </summary>
    /// <typeparam name="UData">Type of Data.</typeparam>
    /// <typeparam name="UAttribute">Type of Attribute.</typeparam>
    public static class Load<UData, UAttribute>
        where UAttribute : IComparable<UAttribute>
    {
        /// <summary>
        /// Loader Event Handler.
        /// </summary>
        /// <param name="sender">The instance of the sender.</param>
        /// <param name="e">Event Arguments.</param>
        public delegate void LoaderEventHandler(object sender, Arguments.DataArgs<UData, UAttribute> e);
        /// <summary>
        /// Crypto Service Event Handler.
        /// </summary>
        /// <param name="sender">The instance of the sender.</param>
        /// <param name="e">Event Arguments.</param>
        public delegate void CryptoEventHandler(object sender, Arguments.CryptoArgs e);
        /// <summary>
        /// On Loaded Event.
        /// </summary>
        public static event LoaderEventHandler Loaded;
        /// <summary>
        /// On Decrypted Event.
        /// </summary>
        public static event CryptoEventHandler Decrypted;

        private static Unit<UData, UAttribute> ArgReturn;
        /// <summary>
        /// Loads a data Unit from a file with encryption.
        /// </summary>
        /// <param name="path">The path of the file.</param>
        /// <returns>The loaded data Unit.</returns>
        public static Unit<UData, UAttribute> load(string path)
        {
            Unit<UData, UAttribute> outUnit = new Unit<UData, UAttribute>();
            uint dataCount = 0;
            string[] splitstr = new string[1];
            string[] allDataArray = File.ReadAllLines(path);
            string allData = string.Empty;
            foreach (string str in allDataArray)
            {
                if (str.Contains("[NULLSTORAGEAPI_NODE]")) { dataCount++; }
                allData += str;
            }
            outUnit.resize(dataCount);
            for (uint i = 0; i < dataCount; i++)
            {
                splitstr[0] = "[NULLSTORAGEAPI_NODE]";
                string[] containment = allData.Split(splitstr, StringSplitOptions.RemoveEmptyEntries);
                splitstr[0] = "[NULLSTORAGEAPI_ARGUMENT]";
                string[] dataAttrArg = containment[i].Split(splitstr, StringSplitOptions.RemoveEmptyEntries);

                string attr = Encoding.ASCII.GetString(Convert.FromBase64String(dataAttrArg[0]));
                string data = Encoding.ASCII.GetString(Convert.FromBase64String(dataAttrArg[1]));

                outUnit.setAttribute(i, outUnit.ConvertToAttribute(attr));
                outUnit.setData(i, outUnit.ConvertToData(data));
            }
            ArgReturn = outUnit;
            OnLoadCompleted();
            return ArgReturn;
        }
        /// <summary>
        /// Loads a data Unit from a file with encryption.
        /// </summary>
        /// <param name="path">The path of the file.</param>
        /// <param name="AES_Key">Encryption Key (256 Bits)</param>
        /// <param name="AES_IV">Encryption Block Size (128 Bits)</param>
        /// <returns>The loaded data Unit.</returns>
        public static Unit<UData, UAttribute> load(string path, string AES_Key, string AES_IV)
        {
            string decrypted = Utility.Crypto.Decrypt.AES_Text(File.ReadAllText(path), AES_Key, AES_IV);
            OnDecryptionCompleted(AES_Key, AES_IV);
            File.WriteAllText(path, decrypted);

            ArgReturn = load(path);
            return ArgReturn;
        }
        private static void OnLoadCompleted()
        {
            Loaded?.Invoke(null, new Arguments.DataArgs<UData, UAttribute>(ArgReturn));
        }
        private static void OnDecryptionCompleted(string key, string iv)
        {
            Decrypted?.Invoke(null, new Arguments.CryptoArgs(key, iv));
        }
    }
}
