﻿/**************************************************************************\
Copyright (c) 2017 Nirex.0@Gmail.Com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
\**************************************************************************/

using System;
using System.IO;
using System.Text;

/// <summary>
/// Core NSA Namespace.
/// </summary>
namespace NSA.Storage
{
    /// <summary>
    /// Saving Class.
    /// </summary>
    /// <typeparam name="UData">Type of Data.</typeparam>
    /// <typeparam name="UAttribute">Type of Attribute.</typeparam>
    public static class Save<UData, UAttribute>
        where UAttribute : IComparable<UAttribute>
    {
        /// <summary>
        /// Saver Event Handler.
        /// </summary>
        /// <param name="sender">The instance of the sender.</param>
        /// <param name="e">Event Arguments.</param>
        public delegate void SaverEventHandler(object sender, Arguments.DataArgs<UData, UAttribute> e);
        /// <summary>
        /// Crypto Service Event Handler.
        /// </summary>
        /// <param name="sender">The instance of the sender.</param>
        /// <param name="e">Event Arguments.</param>
        public delegate void CryptoEventHandler(object sender, Arguments.CryptoArgs e);
        /// <summary>
        /// On Saved Event.
        /// </summary>
        public static event SaverEventHandler Saved;
        /// <summary>
        /// On Encrypted Event.
        /// </summary>
        public static event CryptoEventHandler Encrypted;

        private static Unit<UData, UAttribute> _storage;
        /// <summary>
        /// Saves a data unit into a file.
        /// </summary>
        /// <param name="path">The path of the file.</param>
        /// <param name="intake">The Unit to save into the file.</param>
        public static void save(string path, Unit<UData, UAttribute> intake)
        {
            _storage = new Unit<UData, UAttribute>(intake.Length());
            _storage = intake;

            string dataToSave = string.Empty;
            for (uint i = 0; i < _storage.Length(); i++)
            {
                string attr = Convert.ToBase64String(Encoding.ASCII.GetBytes(_storage.getAttribute(i).ToString()));
                string data = Convert.ToBase64String(Encoding.ASCII.GetBytes(_storage.getData(i).ToString()));

                dataToSave = dataToSave + "\n[NULLSTORAGEAPI_NODE][NULLSTORAGEAPI_ARGUMENT]" + attr + "[NULLSTORAGEAPI_ARGUMENT]" + data + "[NULLSTORAGEAPI_ARGUMENT]";
            }
            OnSaveCompleted(_storage);
            File.WriteAllText(path, dataToSave);
        }
        /// <summary>
        /// Saves a data unit into a file with encryption.
        /// </summary>
        /// <param name="path">The path of the file.</param>
        /// <param name="intake">The Unit to save into the file.</param>
        /// <param name="AES_Key">Encryption Key (256 Bits)</param>
        /// <param name="AES_IV">Encryption Block Size (128 Bits)</param>
        public static void save(string path, Unit<UData, UAttribute> intake, string AES_Key, string AES_IV)
        {
            save(path, intake);
            string encrypted = Utility.Crypto.Encrypt.AES_Text(File.ReadAllText(path), AES_Key, AES_IV);
            OnEncryptionCompleted(AES_Key, AES_IV);
            File.WriteAllText(path, encrypted);
        }
        private static void OnSaveCompleted(Unit<UData,UAttribute> unit)
        {
            Saved?.Invoke(null, new Arguments.DataArgs<UData, UAttribute>(unit));
        }
        private static void OnEncryptionCompleted(string key, string iv)
        {
            Encrypted?.Invoke(null, new Arguments.CryptoArgs(key, iv));
        }
    }
}
