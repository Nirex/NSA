﻿/**************************************************************************\
Copyright (c) 2017 Nirex.0@Gmail.Com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
\**************************************************************************/

using System;

/// <summary>
/// Core NSA Namespace.
/// </summary>
namespace NSA.Storage
{
    /// <summary>
    /// Basic Data unit.
    /// </summary>
    /// <typeparam name="UData">Type of data.</typeparam>
    /// <typeparam name="UAttribute">Type of Attribute given to the data elements.</typeparam>
    public class Unit<UData, UAttribute>
        where UAttribute : IComparable<UAttribute>
    {
        private uint _len;

        private UAttribute[] _attrName;
        private UAttribute[] _attrBuffer;

        private UData[] _data;
        private UData[] _dataBuffer;

        /// <summary>
        /// Basic Data Unit with the length of 0.
        /// </summary>
        public Unit()
        {
            _len = 0;

            _attrName = new UAttribute[0];
            _attrBuffer = new UAttribute[0];

            _data = new UData[0];
            _dataBuffer = new UData[0];
        }
        /// <summary>
        /// Basic Data Unit.
        /// </summary>
        /// <param name="size">Determines the length of the data array.</param>
        public Unit(uint size)
        {
            _len = size;

            _attrName = new UAttribute[size];
            _attrBuffer = new UAttribute[size];

            _data = new UData[size];
            _dataBuffer = new UData[size];
        }
        /// <summary>
        /// Adds a given data and attribute to the end of the data array.
        /// </summary>
        /// <param name="data">Data to add.</param>
        /// <param name="attrib">Attribute to add.</param>
        public void push_back(UData data, UAttribute attrib)
        {
            resize(_len + 1);
            _data[_len - 1] = data;
            _attrName[_len - 1] = attrib;
        }
        /// <summary>
        /// Returns the last element of the data array while deleting it from the array itself.
        /// </summary>
        /// <returns>The last element of the data array.</returns>
        public UData pop_back()
        {
            UData retVal = _data[_len];
            resize(_len - 1);
            return retVal;
        }
        /// <summary>
        /// Gets all the available data elements within the array that have the same attribute as the given attribute.
        /// </summary>
        /// <param name="attrib">Attribute to search for.</param>
        /// <returns>Data array of all the elements with the given attribute.</returns>
        public UData[] getAllDataByAttribute(UAttribute attrib)
        {
            UData[] tmp = new UData[_len];
            uint countIndex = 0;
            for (int i = 0; i < _data.Length; i++)
            {
                if (attrib.CompareTo(_attrName[i]) == 0)
                {
                    tmp[countIndex] = _data[i];
                    countIndex++;
                }
            }
            UData[] retVal = new UData[countIndex];
            Array.Copy(tmp, retVal, countIndex);
            return retVal;
        }
        /// <summary>
        /// Returns the first available data element within the array that have the same attribute as the given attribute.
        /// </summary>
        /// <param name="attrib">Attribute to search for.</param>
        /// <returns>Data element of the first element with the given attribute.</returns>
        public UData getDataByAttribute(UAttribute attrib)
        {
            UData retVal = default(UData);
            for (int i = 0; i < _data.Length; i++)
            {
                if (attrib.CompareTo(_attrName[i]) == 0)
                {
                    retVal = _data[i];
                }
            }
            return retVal;
        }
        /// <summary>
        /// Resizes the data array.
        /// </summary>
        /// <param name="newSize">New array size.</param>
        public void resize(uint newSize)
        {
            uint _sta = 0;

            if (newSize > _len) { _sta = (uint)_data.Length; }
            else { _sta = newSize; }

            _dataBuffer = new UData[_data.Length];
            Array.Copy(_data, _dataBuffer, _data.Length);

            _attrBuffer = new UAttribute[_attrName.Length];
            Array.Copy(_attrName, _attrBuffer, _attrName.Length);

            _data = new UData[newSize];
            Array.Copy(_dataBuffer, _data, _sta);

            _attrName = new UAttribute[newSize];
            Array.Copy(_attrBuffer, _attrName, _sta);

            _dataBuffer = new UData[0];
            _attrBuffer = new UAttribute[0];

            _len = _sta;
        }
        /// <summary>
        /// Gets the attribute of the array at the given index.
        /// </summary>
        /// <param name="index">The index of the attribute.</param>
        /// <returns>Attribute of the data array in the given index.</returns>
        public UAttribute getAttribute(uint index)
        {
            return _attrName[index];
        }
        /// <summary>
        /// Sets the attribute of the array at the given index.
        /// </summary>
        /// <param name="index">The index of the attribute.</param>
        /// <param name="attr">The attribute to set.</param>
        public void setAttribute(uint index, UAttribute attr)
        {
            _attrName[index] = attr;
        }
        /// <summary>
        /// Gets the Data of the array at the given index.
        /// </summary>
        /// <param name="index">The index of the Data.</param>
        /// <returns>Data of the data array in the given index.</returns>
        public UData getData(uint index)
        {
            return _data[index];
        }
        /// <summary>
        /// Sets the Data of the array at the given index.
        /// </summary>
        /// <param name="index">The index of the data.</param>
        /// <param name="data">Data to set.</param>
        public void setData(uint index, UData data)
        {
            _data[index] = data;
        }
        /// <summary>
        /// Gets the Data of the array at the given index.
        /// </summary>
        /// <param name="index">The index of the data.</param>
        /// <returns>Data of the data array in the given index.</returns>
        public UData this[uint index]
        {
            get { return _data[index]; }
        }
        /// <summary>
        /// Sets the Data of the array at the given index.
        /// </summary>
        /// <param name="index">The index of the data.</param>
        /// <param name="data">Data to set.</param>
        /// <returns>Data of the data array in the given index.</returns>
        public UData this[uint index, UData data]
        {
            get { return _data[index]; }
            set { _data[index] = data; }
        }
        /// <summary>
        /// The Length of the data Array.
        /// </summary>
        /// <returns>The number of the elements available in the data array.</returns>
        public uint Length()
        {
            return _len;
        }
        /// <summary>
        /// Converts the given input into the type of the Data.
        /// </summary>
        /// <typeparam name="X">The type of the input.</typeparam>
        /// <param name="intake">The input.</param>
        /// <returns>The given input converted into the type of Data.</returns>
        public UData ConvertToData<X>(X intake)
        {
            return (UData)Convert.ChangeType(intake, typeof(X));
        }
        /// <summary>
        /// Converts the given input into the type of the Attribute.
        /// </summary>
        /// <typeparam name="X">The type of the input.</typeparam>
        /// <param name="intake">The input.</param>
        /// <returns>The given input converted into the type of Attribute.</returns>
        public UAttribute ConvertToAttribute<X>(X intake)
        {
            return (UAttribute)Convert.ChangeType(intake, typeof(X));
        }
    }
}