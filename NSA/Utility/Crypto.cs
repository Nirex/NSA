﻿/**************************************************************************\
Copyright (c) 2017 Nirex.0@Gmail.Com

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
\**************************************************************************/

using System;
using System.Text;
using System.Security.Cryptography;

/// <summary>
/// DO NOT USE THIS DIRECTLY.
/// </summary>
namespace NSA.Utility
{
    /// <summary>
    /// DO NOT USE THIS DIRECTLY.
    /// </summary>
    public class Crypto
    {
        /// <summary>
        /// DO NOT USE THIS DIRECTLY.
        /// </summary>
        public static class Encrypt
        {
            public static byte[] AES_Data(byte[] data, string key, string IV)
            {
                key = IUtility.Shorten(key, 32);
                IV = IUtility.Shorten(IV, 16);
                AesCryptoServiceProvider ACSP = new AesCryptoServiceProvider();
                ACSP.BlockSize = 128;
                ACSP.KeySize = 256;
                ACSP.Key = Encoding.ASCII.GetBytes(key);
                ACSP.IV = Encoding.ASCII.GetBytes(IV);
                ACSP.Padding = PaddingMode.PKCS7;
                ACSP.Mode = CipherMode.CBC;
                ICryptoTransform cryptotransform = ACSP.CreateEncryptor(ACSP.Key, ACSP.IV);
                byte[] encrypted = cryptotransform.TransformFinalBlock(data, 0, data.Length);
                cryptotransform.Dispose();
                return encrypted;
            }
            public static string AES_Text(string input, string key, string IV)
            {
                key = IUtility.Shorten(key, 32);
                IV = IUtility.Shorten(IV, 16);
                byte[] ptb = Encoding.ASCII.GetBytes(input);
                AesCryptoServiceProvider ACSP = new AesCryptoServiceProvider();
                ACSP.BlockSize = 128;
                ACSP.KeySize = 256;
                ACSP.Key = Encoding.ASCII.GetBytes(key);
                ACSP.IV = Encoding.ASCII.GetBytes(IV);
                ACSP.Padding = PaddingMode.PKCS7;
                ACSP.Mode = CipherMode.CBC;
                ICryptoTransform cryptotransform = ACSP.CreateEncryptor(ACSP.Key, ACSP.IV);
                byte[] encrypted = cryptotransform.TransformFinalBlock(ptb, 0, ptb.Length);
                cryptotransform.Dispose();
                return Convert.ToBase64String(encrypted);
            }

        }
        /// <summary>
        /// DO NOT USE THIS DIRECTLY.
        /// </summary>
        public static class Decrypt
        {
            public static byte[] AES_Data(byte[] input, string key, string IV)
            {
                key = IUtility.Shorten(key, 32);
                IV = IUtility.Shorten(IV, 16);
                AesCryptoServiceProvider ACSP = new AesCryptoServiceProvider();
                ACSP.BlockSize = 128;
                ACSP.KeySize = 256;
                ACSP.Key = Encoding.ASCII.GetBytes(key);
                ACSP.IV = Encoding.ASCII.GetBytes(IV);
                ACSP.Padding = PaddingMode.PKCS7;
                ACSP.Mode = CipherMode.CBC;
                ICryptoTransform cryptotransform = ACSP.CreateDecryptor(ACSP.Key, ACSP.IV);
                byte[] decrypted = cryptotransform.TransformFinalBlock(input, 0, input.Length);
                cryptotransform.Dispose();
                return decrypted;
            }
            public static string AES_Text(string input, string key, string IV)
            {
                key = IUtility.Shorten(key, 32);
                IV = IUtility.Shorten(IV, 16);
                byte[] encryptedByte = Convert.FromBase64String(input);
                AesCryptoServiceProvider ACSP = new AesCryptoServiceProvider();
                ACSP.BlockSize = 128;
                ACSP.KeySize = 256;
                ACSP.Key = Encoding.ASCII.GetBytes(key);
                ACSP.IV = Encoding.ASCII.GetBytes(IV);
                ACSP.Padding = PaddingMode.PKCS7;
                ACSP.Mode = CipherMode.CBC;
                ICryptoTransform cryptotransform = ACSP.CreateDecryptor(ACSP.Key, ACSP.IV);
                byte[] decrypted = cryptotransform.TransformFinalBlock(encryptedByte, 0, encryptedByte.Length);
                cryptotransform.Dispose();
                return Encoding.ASCII.GetString(decrypted);
            }
        }

    }
}
